package com.example.shopcommon.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:14
 **/
@Entity(name = "shop_product")
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 商品名
     */
    private String productName;
    /**
     * 价格
     */
    private Double price;
    /**
     * 库存
     */
    private Integer stock;
}
