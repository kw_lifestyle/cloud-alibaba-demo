package com.example.shopcommon.entity;

import lombok.Data;
import org.aopalliance.intercept.Interceptor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:16
 **/
@Entity(name = "shop_order")
@Data
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 用户姓名
     */
    private String userName;
    /**
     * 商品标识
     */
    private Integer productId;
    /**
     * 商品姓名
     */
    private String productName;
    /**
     * 商品价格
     */
    private Double productPrice;
    /**
     * 购买数量
     */
    private Integer number;
}
