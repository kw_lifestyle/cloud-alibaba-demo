package com.example.shopuser.dao;

import com.example.shopcommon.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:27
 **/
public interface UserDao extends JpaRepository<User, Integer> {
}
