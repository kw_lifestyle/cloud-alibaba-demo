package com.example.shopproduct.controller;

import com.example.shopcommon.entity.Product;
import com.example.shopproduct.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:26
 **/
@RestController
@RequestMapping("product")
public class ProductController {

    @Resource
    private ProductService productService;

    @GetMapping("{productId}")
    public Product product(@PathVariable Integer productId) {
        return productService.findByProductId(productId);
    }

    @GetMapping("api1/demo1")
    public String demo1() {
        return "demo1";
    }

    @GetMapping("api1/demo2")
    public String demo2() {
        return "demo2";
    }

    @GetMapping("api2/demo1")
    public String demo3() {
        return "demo1";
    }

    @GetMapping("api2/demo2")
    public String demo4() {
        return "demo1";
    }
}
