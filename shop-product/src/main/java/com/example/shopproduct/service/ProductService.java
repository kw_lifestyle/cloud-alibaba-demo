package com.example.shopproduct.service;

import com.example.shopcommon.entity.Product;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:26
 **/
public interface ProductService {
    /**
     * 根据商品Id 查询商品信息
     *
     * @param productId
     * @return
     */
    Product findByProductId(Integer productId);
}
