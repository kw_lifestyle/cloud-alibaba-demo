package com.example.shopproduct.service.impl;


import com.example.shopcommon.entity.Product;
import com.example.shopproduct.dao.ProductDao;
import com.example.shopproduct.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:26
 **/
@Service

public class ProductServiceImpl implements ProductService {

    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Resource
    private ProductDao productDao;

    @Override
    public Product findByProductId(Integer productId) {
        log.info("查询商品请求参数{}", productId);
        Optional<Product> byId = productDao.findById(productId);
        return byId.orElseGet(Product::new);
    }
}
