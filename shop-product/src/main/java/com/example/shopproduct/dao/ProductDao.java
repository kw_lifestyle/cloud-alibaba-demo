package com.example.shopproduct.dao;

import com.example.shopcommon.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:27
 **/
@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {
}
