package com.example.shoporder.config;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
import com.sun.deploy.security.BlockedException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 09 14:23
 **/
public class ExceptionHandlerPage implements BlockExceptionHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, BlockException ex) throws Exception {
//        String msg = null;
//        if (ex instanceof FlowException) {
//            msg = "限流了";
//        } else if (ex instanceof DegradeException) {
//            msg = "降级了";
//        } else if (ex instanceof ParamFlowException) {
//            msg = "热点参数限流";
//        } else if (ex instanceof SystemBlockException) {
//            msg = "系统规则（负载/...不满足要求）";
//        } else if (ex instanceof AuthorityException) {
//            msg = "授权规则不通过";
//        }
    }
}
