package com.example.shoporder.dao;

import com.example.shopcommon.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:27
 **/
@Repository
public interface OrderDao extends JpaRepository<Order, Integer> {
}
