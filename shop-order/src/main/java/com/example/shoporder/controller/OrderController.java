package com.example.shoporder.controller;

import com.example.shopcommon.entity.Order;
import com.example.shoporder.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:26
 **/
@RestController
@RequestMapping("order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("{productId}")
    public Order order(@PathVariable Integer productId) {
        return orderService.createOrder(productId);
    }
}
