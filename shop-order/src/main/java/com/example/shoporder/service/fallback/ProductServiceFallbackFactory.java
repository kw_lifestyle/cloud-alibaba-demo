package com.example.shoporder.service.fallback;

import com.example.shopcommon.entity.Product;
import com.example.shoporder.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description 容错工厂类，实现接口FallbackFactory
 * @create 2024 - 06 - 09 16:15
 **/
@Component
public class ProductServiceFallbackFactory implements FallbackFactory<ProductService> {
    private static final Logger log = LoggerFactory.getLogger(ProductServiceFallbackFactory.class);

    @Override
    public ProductService create(Throwable cause) {
        log.info("feign 异常", cause);
        return productId -> {
            // 容错逻辑
            Product product = new Product();
            product.setId(-1);
            product.setProductName("远程调用商品异常");
            return product;
        };
    }
}
