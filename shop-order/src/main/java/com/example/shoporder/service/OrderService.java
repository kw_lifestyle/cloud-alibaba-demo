package com.example.shoporder.service;

import com.example.shopcommon.entity.Order;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 08 18:26
 **/
public interface OrderService {
    Order createOrder(Integer productId);
}
