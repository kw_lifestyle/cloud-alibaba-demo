package com.example.shoporder.service;

import com.example.shopcommon.entity.Product;
import com.example.shoporder.service.fallback.ProductServiceFallback;
import com.example.shoporder.service.fallback.ProductServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description
 * @create 2024 - 06 - 09 10:08
 **/
// 指定value属性
@FeignClient(value = "service-product",
        fallback = ProductServiceFallback.class
//        fallbackFactory = ProductServiceFallbackFactory.class
)
public interface ProductService {

    @GetMapping("product/{productId}")
    Product findByProductId(@PathVariable Integer productId);
}
