package com.example.shoporder.service.fallback;

import com.example.shopcommon.entity.Product;
import com.example.shoporder.service.ProductService;
import org.springframework.stereotype.Component;

/**
 * @author kw
 * @program springcloud-alibaba
 * @description 容错类 ，当feign调用远程异常，则进入容错逻辑
 * @create 2024 - 06 - 09 16:15
 **/
@Component
public class ProductServiceFallback implements ProductService {
    @Override
    public Product findByProductId(Integer productId) {
        // 容错逻辑
        Product product = new Product();
        product.setId(-1);
        product.setProductName("远程调用商品异常");
        return product;
    }
}
